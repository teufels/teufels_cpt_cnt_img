<?php
namespace TEUFELS\TeufelsCptCntImg\Tests\Unit\Controller;
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 Andreas Hafner <a.hafner@teufels.com>, teufels GmbH
 *  			Dominik Hilser <d.hilser@teufels.com>, teufels GmbH
 *  			Georg Kathan <g.kathan@teufels.com>, teufels GmbH
 *  			Hendrik Krüger <h.krueger@teufels.com>, teufels GmbH
 *  			Perrin Ennen <p.ennen@teufels.com>, teufels GmbH
 *  			Timo Bittner <t.bittner@teufels.com>, teufels GmbH
 *  			
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class TEUFELS\TeufelsCptCntImg\Controller\FalImageController.
 *
 * @author Andreas Hafner <a.hafner@teufels.com>
 * @author Dominik Hilser <d.hilser@teufels.com>
 * @author Georg Kathan <g.kathan@teufels.com>
 * @author Hendrik Krüger <h.krueger@teufels.com>
 * @author Perrin Ennen <p.ennen@teufels.com>
 * @author Timo Bittner <t.bittner@teufels.com>
 */
class FalImageControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{

	/**
	 * @var \TEUFELS\TeufelsCptCntImg\Controller\FalImageController
	 */
	protected $subject = NULL;

	public function setUp()
	{
		$this->subject = $this->getMock('TEUFELS\\TeufelsCptCntImg\\Controller\\FalImageController', array('redirect', 'forward', 'addFlashMessage'), array(), '', FALSE);
	}

	public function tearDown()
	{
		unset($this->subject);
	}

	/**
	 * @test
	 */
	public function showActionAssignsTheGivenFalImageToView()
	{
		$falImage = new \TEUFELS\TeufelsCptCntImg\Domain\Model\FalImage();

		$view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
		$this->inject($this->subject, 'view', $view);
		$view->expects($this->once())->method('assign')->with('falImage', $falImage);

		$this->subject->showAction($falImage);
	}
}
