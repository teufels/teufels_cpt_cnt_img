<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'TEUFELS.' . $_EXTKEY,
	'Teufelscptcntimgshowfalimage',
	array(
		'FalImage' => 'showFalImage',
		
	),
	// non-cacheable actions
	array(
		'FalImage' => '',
		
	)
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
	'
	mod {
    wizards.newContentElement.wizardItems.common {
      elements {
        teufels_cpt_cnt_img {
          icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_teufelscptcntimg_domain_model_falimage.png
          title = teufels FAL Image
          description = Pure FAL Image without Flux
          tt_content_defValues {
            CType = list
	     	list_type = teufelscptcntimg_teufelscptcntimgshowfalimage
          }
        }
      }
      show := addToList(teufels_cpt_cnt_img)
    }
  }
  '
);