<?php
namespace TEUFELS\TeufelsCptCntImg\Controller;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Resource\FileInterface;
use TYPO3\CMS\Core\Resource\FileReference;

/**
 * FalImageController
 */
class FalImageController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * falImageRepository
     *
     * @var \TEUFELS\TeufelsCptCntImg\Domain\Repository\FalImageRepository
     * @inject
     */
    protected $falImageRepository = NULL;

    /**
     * @var \TYPO3\CMS\Extbase\Service\ImageService
     */
    protected $imageService;

    /**
     * @param \TYPO3\CMS\Extbase\Service\ImageService $imageService
     */
    public function injectImageService(\TYPO3\CMS\Extbase\Service\ImageService $imageService)
    {
        $this->imageService = $imageService;
    }
    
    /**
     * action showFalImage
     *
     * @return void
     */
    public function showFalImageAction()
    {
        $aSettings = $this->settings;
        $iFalImageUid = $this->settings['falimage'];

        if(!empty($iFalImageUid)){
            $resourceFactory = GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Resource\\ResourceFactory');
            $oCoreFileReference = $resourceFactory->getFileReferenceObject($iFalImageUid);

            $aFigure = array();

            if ($oCoreFileReference != NULL && get_class($oCoreFileReference) == "TYPO3\CMS\Core\Resource\FileReference") {

                $processingInstructions = array(
                    'maxWidth' => $aSettings['maxWidth'],
                    'maxHeight' => $aSettings['maxHeight'],
                    'crop' => $oCoreFileReference instanceof FileReference ? $oCoreFileReference->getProperty('crop') : null,
                );
                
                $processedImage = $this->imageService->applyProcessingInstructions($oCoreFileReference, $processingInstructions);
                $sUrl = $this->imageService->getImageUri($processedImage);

                $aFigure['oCoreFileReference'] = $oCoreFileReference;
                $aFigure['sId'] = md5($sUrl) . rand(0,99999);
                $aFigure['sUrl'] = $sUrl;
                $aFigure['sSrc'] = "data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==";

                $aFigure['iWidth'] = $processedImage->getProperty('width');
                $aFigure['iHeight'] = $processedImage->getProperty('height');
                $aFigure['sStyle'] = ($aFigure['iWidth'] < $aFigure['iHeight'] ? ' style="max-height: 100%; ' : '');
                
            }

        }

        $this->view->assign('aSettings', $aSettings);
        $this->view->assign('aFigure', $aFigure);
        $this->view->assign('bDebug', $aSettings['bDebug']);
    }

}