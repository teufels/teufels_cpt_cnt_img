<?php
namespace TEUFELS\TeufelsCptCntImg\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * FalImage
 */
class FalImage extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * falImage
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     */
    protected $falImage = null;
    
    /**
     * focusX
     *
     * @var float
     */
    protected $focusX = 0.0;
    
    /**
     * focusY
     *
     * @var float
     */
    protected $focusY = 0.0;
    
    /**
     * Returns the falImage
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $falImage
     */
    public function getFalImage()
    {
        return $this->falImage;
    }
    
    /**
     * Sets the falImage
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $falImage
     * @return void
     */
    public function setFalImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $falImage)
    {
        $this->falImage = $falImage;
    }
    
    /**
     * Returns the focusX
     *
     * @return float $focusX
     */
    public function getFocusX()
    {
        return $this->focusX;
    }
    
    /**
     * Sets the focusX
     *
     * @param float $focusX
     * @return void
     */
    public function setFocusX($focusX)
    {
        $this->focusX = $focusX;
    }
    
    /**
     * Returns the focusY
     *
     * @return float $focusY
     */
    public function getFocusY()
    {
        return $this->focusY;
    }
    
    /**
     * Sets the focusY
     *
     * @param float $focusY
     * @return void
     */
    public function setFocusY($focusY)
    {
        $this->focusY = $focusY;
    }

}